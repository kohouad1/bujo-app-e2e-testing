require("dotenv").config();
const express = require("express");
const entries = require("./api/entries/entries");
const users = require("./api/users/users");
const app = express();
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const Auth = require("./auth");

// Use parsers
app.use(cookieParser()); // for getting request cookies in a convenient way
app.use(bodyParser.json()); // for sending JSON
app.use(bodyParser.urlencoded({ extended: true })); // for sending Form Data

// Set API routes for users and entries
app.use("/api/users", users);
app.use("/api/entries", entries);

// Connect to the database
require("./db/connection");

// Prevent getting "cannot GET" error when user refreshes the page in the application
["/app/monthly-log", "/app/reporting"].forEach((path) => {
  app.get(path, (req, res) => res.redirect("/app"));
});
app.all("/", (req, res) => { res.redirect("/home") });
app.get("/app", checkAuthApp, (req, res) => res.sendFile(`${__dirname}/public/app/index.html`));
app.get("/home", checkAuthHome, (req, res) => res.sendFile(`${__dirname}/public/home/index.html`));
app.use(express.static(__dirname + "/public/"));

// Check if the user is allowed to visit the application, if not,
// redirect them to the homepage
function checkAuthApp(req, res, next) {
  if (Auth.verifyToken(Auth.getAccessTokenFromRequest(req))) {
    return next();
  } else {
    let authData = Auth.refreshAccessToken(req, res);
    if (authData) {
      next()
    } else {
      return res.redirect("/home");
    }
  }
}

// If the user tries to visit the homepage and they are logged in,
// redirect them to the application
function checkAuthHome(req, res, next) {
  if (Auth.verifyToken(Auth.getAccessTokenFromRequest(req))) {
    return res.redirect("/app");
  } else {
    let authData = Auth.refreshAccessToken(req, res);
    if (authData) {
      return res.redirect("/app");
    } else {
      return next();
    }
  }
}

// Start listening on port
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server listening on port ${port}...`));
