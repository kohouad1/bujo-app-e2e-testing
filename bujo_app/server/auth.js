require("dotenv").config();
const jwt = require("jsonwebtoken");

module.exports.signInUser = function (req, res, user) {
  const accessToken = this.generateToken(user);
  const refreshToken = this.generateToken(user,true);
  res.cookie("access_token", accessToken, { httpOnly: true });
  res.cookie("refresh_token", refreshToken, { httpOnly: true });
  req.cookies["access_token"] = accessToken;
  req.cookies["refresh_token"] = refreshToken;
};

module.exports.authToken = function (req, res, next) {
  let authData = this.verifyToken(this.getAccessTokenFromRequest(req));
  if (authData) {
    req.authData = authData;
    next();
  } else {
    authData = this.refreshAccessToken(req, res);
    if (authData) {
      req.authData = authData;
      next();
    } else {
      return res.status(403).send();
    }
  }
};

module.exports.refreshAccessToken = function (req, res) {
  const authData = this.verifyToken(this.getRefreshTokenFromRequest(req), true);
  if (authData) {
    const { _id, username } = authData;
    res.cookie("access_token", this.generateToken({ _id, username }), { httpOnly: true });
    console.log("Access token was successfully refreshed.");
    return authData;
  }
  console.log("Refresh token expired.");
  return undefined;
};

module.exports.generateToken = function (user, isRefresh = false) {
  try {
    const expiresIn = isRefresh ? "1d" : "15m";
    return jwt.sign({
      _id: user._id,
      username: user.username
    }, isRefresh ? process.env.JWT_REFRESH_SECRET : process.env.JWT_ACCESS_SECRET, { expiresIn });
  } catch (e) {
    console.log(`Error generating token: ${e.name} - ${e.message}`);
    return undefined;
  }
};

module.exports.verifyToken = function (token, isRefresh = false) {
  try {
    return jwt.verify(token, isRefresh ? process.env.JWT_REFRESH_SECRET : process.env.JWT_ACCESS_SECRET);
  } catch (e) {
    console.log(`Error verifying token: ${e.name} - ${e.message}`);
    return undefined;
  }
};

module.exports.getAccessTokenFromRequest = function (req) {
  return req.cookies["access_token"];
};

module.exports.getRefreshTokenFromRequest = function (req) {
  return req.cookies["refresh_token"];
};
