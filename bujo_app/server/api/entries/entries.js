require("dotenv").config();
const express = require("express");
const router = express.Router();
const Entry = require("../../db/models/Entry");
const moment = require("moment");
const Auth = require("../../auth");

// Authenticate sent token for every request regarding entries
router.use((req, res, next) => { Auth.authToken(req, res, next); });

// ENTRIES ROUTES
// fetchEntriesByYearAndMonth
router.get("/:year/:month", function (req, res) {
  const year = req.params.year;
  const month = req.params.month - 1;
  const startOfMonth = moment().year(year).month(month).startOf("month").toDate();
  const endOfMonth = moment().year(year).month(month).endOf("month").toDate();
  const userId = req.authData._id;
  const query = { userId, dateDailyLog: { $gte: startOfMonth, $lte: endOfMonth } };
  Entry.find(query, (err, docs) => {
    if (err) {
      res.status(400).send({
        message: err
      });
      return console.log(err);
    }
    res.status(200).send({
      message: "Successfully fetched entries.",
      resources: docs
    })
  });
});

// fetchEntriesByTimeSpan
router.get("/", function (req, res) {
  if (!req.query.fromDate || !req.query.toDate) {
    return res.status(422).send({
      message: "Missing fromDate and/or toDate parameter(s)."
    });
  }
  const fromDate = moment(req.query.fromDate).startOf("day").toDate();
  const toDate = moment(req.query.toDate).endOf("day").toDate();
  const userId = req.authData._id;
  const query = { userId , dateDailyLog: { $gte: fromDate, $lte: toDate } };
  Entry.find(query, null, { sort: { dateDailyLog: 1 } }, (err, docs) => {
    if (err) {
      res.status(400).send({
        message: err
      });
      return console.log(err);
    }
    res.status(200).send({
      message: "Successfully fetched entries.",
      resources: docs
    });
  });
});

// createNewEntry
router.post("/", function (req, res) {
  req.body.userId = req.authData._id;
  const newEntry = new Entry(req.body);
  newEntry.save((err, newEntry) => {
    if (err) {
      res.status(400).send({
        message: err
      });
      return console.error(err);
    }
    res.status(201).send({
      message: "Successfully inserted new entry.",
      resource: newEntry
    })
  });
});

// createNewSubEntry
router.post("/:parentId/subentries", function (req, res) {
  const parentId = req.params.parentId;
  const newSubEntry = req.body;
  newSubEntry.userId = req.authData._id;
  const query = { _id: parentId };
  Entry.findOneAndUpdate(query, { $push: { childEntries: newSubEntry } }, (err, doc) => {
    if (err) {
      res.status(400).send(err);
      return console.log(err);
    }
    res.status(201).send({
      message: `Successfully created a new sub-entry with parent id: ${parentId}`,
      resource: doc
    });
  });
});

// updateEntry
router.patch("/:id", function (req, res) {
  const id = req.params.id;
  const updatedEntry = req.body;
  updatedEntry.userId = req.authData._id;
  Entry.findByIdAndUpdate(id, updatedEntry, (err, doc) => {
    if (err) {
      res.status(400).send(err);
      return console.log(err);
    }
    doc.text = updatedEntry.text;
    res.status(200).send({
      message: `Successfully updated existing entry with id: ${id}.`,
      resource: doc
    });
  });
});

// updateSubEntry
router.patch("/:parentId/subentries/:id", function (req, res) {
  const parentId = req.params.parentId;
  const subEntryId = req.params.id;
  const updatedSubEntry = req.body;
  updatedSubEntry.userId = req.authData._id;
  const query = { _id: parentId, "childEntries._id": subEntryId };
  Entry.findOneAndUpdate(query, { $set: { "childEntries.$": updatedSubEntry } }, (err, doc) => {
    if (err) {
      res.status(400).send(err);
      return console.log(err);
    }
    res.status(200).send({
      message: `Successfully updated existing sub-entry with id: ${subEntryId}.`,
      resource: doc
    });
  })
});

// deleteEntry
router.delete("/:id", function (req, res) {
  const id = req.params.id;
  const userId = req.authData._id;
  const query = { _id: id, userId };
  Entry.deleteOne(query, (err) => {
    if (err) {
      res.status(400).send(err);
      return console.log(err);
    }
    res.status(200).send({
      message: `Successfully deleted entry with id: ${id}.`
    });
  })
});

// deleteSubEntry
router.delete("/:parentId/subentries/:id", function (req, res) {
  const parentId = req.params.parentId;
  const subEntryId = req.params.id;
  const userId = req.authData._id;
  Entry.findByIdAndUpdate(parentId, { $pull: { childEntries: { _id: subEntryId, userId } } }, (err) => {
    if (err) {
      res.status(400).send(err);
      return console.log(err);
    }
    res.status(200).send({
      message: `Successfully deleted sub-entry with id: ${subEntryId}.`
    })
  })
});

module.exports = router;
