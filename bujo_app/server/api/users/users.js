const express = require("express");
const router = express.Router();
const signup = require("./signup");
const signin = require("./signin");
const signout = require("./signout");
const User = require("../../db/models/User");
const Auth = require("../../auth");


router.get("/current", (req, res, next) => { Auth.authToken(req, res, next) }, function (req, res) {
  const userId = req.authData._id;
  const query = { _id: userId };
  User.find(query, (err, docs) => {
    if (err) {
      res.status(400).send({
        message: err
      });
      return console.log(err);
    }
    if (docs.length === 0) {
      return res.status(404).send({
        message: "The user does not exist."
      });
    }
    let user = docs[0];
    res.status(200).send({
      message: "Successfully found the user.",
      resource: user
    });
  });
});

router.use(signup);
router.use(signin);
router.use(signout);

module.exports = router;
