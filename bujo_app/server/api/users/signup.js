const express = require("express");
const router = express.Router();
const upload = require("multer")();
const bcrypt = require("bcrypt");
const Auth = require("../../auth");
const User = require("../../db/models/User");

router.post("/signup", upload.none(), function (req, res) {
  if (!req.body.username || !req.body.password) {
    return res.status(422).send({
      message: "Username and password parameters are required."
    });
  }
  if (!validateUsername(req.body.username)) return res.status(422).send({
    message: "Username validation failed."
  });
  if (!validatePassword(req.body.password)) return res.status(422).send({
    message: "Password validation failed."
  });
  let username = req.body.username;
  let password = req.body.password;
  bcrypt.hash(password, 10, (err, hash) => {
    if (err) {
      return res.status(500).send({
        message: err
      });
    } else {
      User.create({
        username: username,
        password: hash
      }, (err, newUser) => {
        if (err) {
          let status = 400;
          if (err.code === 11000) { // duplicate key, i.e. the username already exists
            status = 409;
          }
          return res.status(status).send({
            message: err
          })
        }
        Auth.signInUser(req, res, newUser);
        return res.send({
          message: "Signed up user successfully."
        })
      });
    }
  });
});

function validateUsername(username) {
 return username.length > 1 && username.length <= 20 && username.replace(/\s/g,'').length > 0;
}

function validatePassword(password) {
  return !!password.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d!$%@#£€*?&]{8,}$/);
}

module.exports = router;
