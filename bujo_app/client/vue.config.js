const path = require("path");

module.exports = {
  outputDir: path.resolve(__dirname, "../server/public"),
  devServer: {
    proxy: {
      "/api": {
        target: "http://localhost:5000"
      }
    }
  },
  pages: {
    app: {
      entry: "src/app/main.js",
      template: "public/app.html",
      filename: "app/index.html"
    },
    home: {
      entry: "src/home/main.js",
      template: "public/home.html",
      filename: "home/index.html"
    }
  }
};
