import Vue from 'vue'
import VueRouter from 'vue-router'
import DailyLog from '../views/DailyLog.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Daily Log',
    component: DailyLog
  },
  {
    path: '/monthly-log',
    name: 'Monthly Log',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/MonthlyLog.vue')
  },
  {
    path: '/reporting',
    name: 'Reporting',
    component: () => import('../views/Reporting.vue')
  },
  {
    path: "*",
    redirect: "/",
    component: DailyLog
  }
];

const router = new VueRouter({
  base: "/app",
  routes,
  mode: "history"
});

export default router
