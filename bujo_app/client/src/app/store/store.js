import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import moment from "moment";

Vue.use(Vuex);

const apiPath = "/api";
const entriesPath = apiPath + "/entries";

export default new Vuex.Store({
  state: {
    currentUser: {},
    entries: [],
    report: {
      type: null,
      entries: [],
      fromDate: null,
      toDate: null,
      statistics: null,
    },
    selectedDateDailyLog: moment().locale("cs"),
    selectedDateMonthlyLog: moment().locale("cs"),
    markedEntry: {},
    markedParentEntry: {},
    isSubEntryOriented: false,
    contentLoading: false
  },
  getters: {
    getCurrentUser(state) {
      return state.currentUser;
    },
    getAllEntries(state) {
      return state.entries;
    },
    getReportType(state) {
      return state.report.type;
    },
    getReportEntries(state) {
      return state.report.entries;
    },
    getReportTimeSpan({ report }) {
      return { fromDate: report.fromDate, toDate: report.toDate };
    },
    getReportStatistics({ report }) {
      return report.statistics;
    },
    getSelectedDateDailyLog(state) {
      return state.selectedDateDailyLog;
    },
    getSelectedDateMonthlyLog(state) {
      return state.selectedDateMonthlyLog;
    },
    getEntriesByDay(state) {
      return function (date) {
        let entries = [];
        for (let entry of state.entries) {
          if (entry.dateDailyLog && moment(entry.dateDailyLog).isSame(date, "day")) {
            entries.push(entry);
          }
        }
        return entries;
      }
    },
    getEntriesByMonth(state) {
      return function (date) {
        let entries = [];
        for (let entry of state.entries) {
          if (entry.dateDailyLog && moment(entry.dateDailyLog).isSame(date, "month")) {
            entries.push(entry);
          }
        }
        return entries;
      }
    },
    getSelectedDateDailyLogEntriesByDay(state, getters) {
      return getters.getEntriesByDay(getters.getSelectedDateDailyLog);
    },
    getSelectedDateDailyLogEntriesByMonth(state, getters) {
      return getters.getEntriesByMonth(getters.getSelectedDateDailyLog);
    },
    getMonthlyLogEntriesByDay(state) {
      return function(date) {
        let entries = [];
        for (let entry of state.entries) {
          if (entry.dateMonthlyLog && moment(entry.dateMonthlyLog).isSame(date, "day")) {
            entries.push(entry);
          }
        }
        return entries;
      }
    },
    getMarkedEntry(state) {
      return state.markedEntry;
    },
    getMarkedParentEntry(state) {
      return state.markedParentEntry;
    },
    getIsSubEntryOriented(state) {
      return state.isSubEntryOriented;
    },
    getContentLoading(state) {
      return state.contentLoading;
    }
  },
  actions: {
    updateCurrentUser({ commit }, user) {
      commit("SET_CURRENT_USER", user);
    },
    async fetchEntriesByYearAndMonth(date) {
      date = moment(date);
      return await axios.get(entriesPath + `/${date.format("YYYY")}/${date.format("M")}`);
    },
    async fetchEntriesByTimeSpan({ commit }, { fromDate, toDate }) {
      commit("DO_NOTHING");
      return await axios.get(entriesPath, {params: {fromDate, toDate}});
    },
    async createNewEntry({ commit }, newEntry) {
      try {
        let response = await axios.post(entriesPath, newEntry);
        if (response.status === 200) {
          commit("ADD_ENTRY", response.data.resource);
        }
        return response;
      } catch (e) {
        return e.response;
      }
    },
    async createNewSubEntry({ commit }, newSubEntry) {
      try {
        return await axios.post(`${entriesPath}/${newSubEntry.parentEntry}/subentries`, newSubEntry);
      } catch (e) {
        return e.response;
      } finally {
        commit("DO_NOTHING");
      }
    },
    async updateEntry({ commit }, entry) {
      try {
        return await axios.patch(`${entriesPath}/${entry._id}`, entry);
      } catch (e) {
        return e.response;
      } finally {
        commit("DO_NOTHING");
      }
    },
    async updateSubEntry({ commit }, subEntry) {
      try {
        return await axios.patch(`${entriesPath}/${subEntry.parentEntry}/subentries/${subEntry._id}`, subEntry);
      } catch (e) {
        return e.response;
      } finally {
        commit("DO_NOTHING");
      }
    },
    async deleteEntry({ commit }, id) {
      try {
        return await axios.delete(`${entriesPath}/${id}`);
      } catch (e) {
        return e.response;
      } finally {
        commit("DO_NOTHING");
      }
    },
    async deleteSubEntry({ commit }, subEntry) {
      try {
        return await axios.delete(`${entriesPath}/${subEntry.parentEntry}/subentries/${subEntry._id}`);
      } catch (e) {
        return e.response;
      } finally {
        commit("DO_NOTHING");
      }
    },
    updateSelectedDateDailyLog({ commit }, newDate) {
      commit("SET_SELECTED_DATE_DAILY_LOG", newDate);
    },
    updateSelectedDateMonthlyLog({ commit }, newDate) {
      commit("SET_SELECTED_DATE_MONTHLY_LOG", moment(newDate).locale("cs"));
    },
    updateMarkedEntry({ commit }, { newMarkedEntry, newMarkedParentEntry, isSubEntryOriented }) {
      if (isSubEntryOriented !== undefined && isSubEntryOriented !== null) {
        commit("SET_IS_SUB_ENTRY_ORIENTED", isSubEntryOriented);
      }
      if (newMarkedEntry) {
        commit("SET_MARKED_ENTRY", newMarkedEntry);
      }
      if (newMarkedParentEntry) {
        commit("SET_MARKED_PARENT_ENTRY", newMarkedParentEntry);
      }
    },
    async updateDailyLogContent({ commit, dispatch, getters }) {
      commit("SET_CONTENT_LOADING", true);
      try {
        const response = await dispatch("fetchEntriesByYearAndMonth", getters.getSelectedDateDailyLog);
        commit("SET_ALL_ENTRIES", response.data.resources);
      } catch (e) {
        console.log(e);
      } finally {
        commit("SET_CONTENT_LOADING", false);
      }
    },
    async updateReportingContent({ commit, dispatch }, { fromDate, toDate, type }) {
      commit("SET_CONTENT_LOADING", true);
      try {
        const response = await dispatch("fetchEntriesByTimeSpan", { fromDate, toDate });
        commit("SET_REPORT_TYPE", type);
        commit("SET_REPORT_ENTRIES", response.data.resources);
        commit("SET_REPORT_TIME_SPAN", { fromDate, toDate });
      } catch (e) {
        console.log(e);
      } finally {
        commit("SET_CONTENT_LOADING", false);
      }
    },
    updateReportStatistics({ commit }, statistics) {
      commit("SET_REPORT_STATISTICS", statistics);
    },
    updateContentLoading({ commit }, newState) {
      commit("SET_CONTENT_LOADING", newState);
    }
  },
  mutations: {
    SET_CURRENT_USER(state, user) {
      state.currentUser = user;
    },
    SET_ALL_ENTRIES(state, entries) {
      state.entries = entries;
    },
    SET_REPORT_TYPE(state, type) {
      state.report.type = type;
    },
    SET_REPORT_ENTRIES(state, reportEntries) {
      state.report.entries = reportEntries;
    },
    SET_REPORT_TIME_SPAN(state, { fromDate, toDate }) {
      state.report.fromDate = fromDate;
      state.report.toDate = toDate;
    },
    SET_REPORT_STATISTICS(state, statistics) {
      state.report.statistics = statistics;
    },
    ADD_ENTRY(state, entry) {
      state.entries.push(entry);
    },
    SET_SELECTED_DATE_DAILY_LOG(state, newDate) {
      state.selectedDateDailyLog = newDate;
    },
    SET_SELECTED_DATE_MONTHLY_LOG(state, newDate) {
      state.selectedDateMonthlyLog = newDate;
    },
    SET_MARKED_ENTRY(state, newMarkedEntry) {
      state.markedEntry = newMarkedEntry;
    },
    SET_MARKED_PARENT_ENTRY(state, newMarkedParentEntry) {
      state.markedParentEntry = newMarkedParentEntry;
    },
    SET_IS_SUB_ENTRY_ORIENTED(state, value) {
      state.isSubEntryOriented = value;
    },
    SET_CONTENT_LOADING(state, value) {
      state.contentLoading = value;
    },
    DO_NOTHING() {}
  }
});
