/// <reference types="cypress" />

import DailyLogPage from "../support/pages/DailyLogPage";

describe("Deleting entries tests", () => {

  const dailyLogPage = new DailyLogPage();
  const entryIdMap = {};
  const entriesToDelete = ["Dojít nakoupit"]

  before(() => {
    cy.fixture("testEntries1.json").as("testEntries");
    cy.signin();
    cy.get("@testEntries").then(testEntries => cy.setupEntries(testEntries, entryIdMap));
    dailyLogPage.visit();
  });

  it("should delete one entry and its subentries", () => {
    for (const entryToDelete of entriesToDelete) {
      cy.get(`#${entryIdMap[entryToDelete]} button.btn-outline-danger`).click();
      cy.get("#confirm-dialog button.btn-danger").click();
    }
    dailyLogPage.getEntryList().should("have.length", 1);
    cy.wrap(true).as("finished");
  });

  after(() => {
    cy.get("@finished").then(() => {
      cy.request("DELETE", `/api/entries/${entryIdMap["Online schůze"]}`);
    });
  });

});
