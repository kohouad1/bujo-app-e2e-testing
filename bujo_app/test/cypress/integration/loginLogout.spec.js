/// <reference types="cypress" />

describe("Login and logout tests", () => {

  before(() => {
    cy.visit("/home");
  });

  it("should login successfully", () => {
    cy.get("#username-input").type("zks_user");
    cy.get("#password-input").type("E2ETesting");
    cy.get("#form-wrapper button[type=submit]").click();

    cy.url().should("include", "/app");
  });

  it("should logout successfully", () => {
    cy.get("#btn-logout").click();

    cy.url().should("include", "/home");
  });

});
