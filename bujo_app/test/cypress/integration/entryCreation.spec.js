/// <reference types="cypress" />

import DailyLogPage from "../support/pages/DailyLogPage";

describe("Creating entries tests", () => {

  const dailyLogPage = new DailyLogPage();
  const entryIdMap = {}; // for finding the correct button in DOM for newly created subentries

  before(() => {
    // listen for incoming responses containing dynamically generated IDs of created entries
    cy.intercept({ method: "POST", pathname: "/api/entries", }).as("createdEntryReq");
    cy.intercept({ method: "POST", pathname: "/api/entries/*/subentries", }).as("createdSubEntryReq");
    cy.fixture("testEntries1.json").as("testEntries");
    cy.signin();
    dailyLogPage.visit();
  });

  it('should successfully create entries', function () {
    cy.wrap(true).as("idSet"); // initialize alias at the beginning
    for (const entry of this.testEntries) {
      createEntry(entry);
    }
    dailyLogPage.getEntryList().should("have.length", 4);
    cy.wrap(true).as("finished");
  });

  after(() => {
    // make sure the testing has finished so the data created during the tests
    // can be cleaned up
    cy.get("@finished").then(() => {
      cy.request("DELETE", `/api/entries/${entryIdMap["Dojít nakoupit"]}`);
      cy.request("DELETE", `/api/entries/${entryIdMap["Online schůze"]}`);
    });
  });

  function createEntry(entry) {
    // make sure the ID of the last created entry is set so it can be used
    // for creating a potential subentry under this entry
    cy.get("@idSet")
      .then(id => {
        if (!entry.parent) dailyLogPage.openNewEntryDialog();
        else               dailyLogPage.openNewSubentryDialog(id);
        dailyLogPage.typeEntryText(entry.text);
        dailyLogPage.setEntryType(["TASK", "TASK_COMPLETED"].includes(entry.type) ? "TASK" : entry.type);
        dailyLogPage.setTaskStatus(entry.type === "TASK_COMPLETED");
        dailyLogPage.setEntryStatus(entry.canceled);
        dailyLogPage.setSignifier(entry.signifier.length !== 0, entry.signifier);
        dailyLogPage.setCopyToMonthlyLog(entry.dateMonthlyLog);

        dailyLogPage.getCreateEntryButton().click();


        return cy.wait(`@created${entry.parent ? "Sub" : ""}EntryReq`)
      })
      // store dynamically generated ID of the new entry for later use
      .then(req => {
        entryIdMap[entry.text] = req.response.body.resource._id;
        cy.wrap(entryIdMap[entry.text]).as("idSet");
      });
  }

});
