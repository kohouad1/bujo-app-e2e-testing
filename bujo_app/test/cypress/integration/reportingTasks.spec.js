/// <reference types="cypress" />

describe("Reporting - Task overview tests", () => {

  const entryIdMap = {};

  before(() => {
    cy.fixture("testEntries1.json").as("testEntries");
    cy.signin();
    cy.get("@testEntries").then(testEntries => cy.setupEntries(testEntries, entryIdMap));
    cy.visit("/app");
  });

  it("should display task overview report correctly", () => {
    cy.get("#header-dropdown-container").click();
    cy.get("#header-dropdown-container ul[role=menu] li:nth-child(3)").click();
    cy.get("#panel hr + button.btn-success").click();
    let valuesWrapper = cy.get("#values-wrapper");
    valuesWrapper.within($wrapper => {
      const expectedValues = [3, 2, 1, 0];
      cy.get("strong")
        .each(($value, index) =>
          expect($value).to.contain(expectedValues[index]));
    });

    cy.wrap(true).as("finished");
  });

  after(() => {
    cy.get("@finished")
      .then(() => cy.get("@testEntries"))
      .then(testEntries => cy.destroyEntries(testEntries, entryIdMap));
  });

});
