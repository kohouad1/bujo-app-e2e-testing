/// <reference types="cypress" />

describe("Entries moved to Monthly Log tests" , () => {

  const entryIdMap = {};

  before(() => {
    cy.fixture("testEntries2.json").as("testEntries");
    cy.signin();
    cy.get("@testEntries").then(testEntries => cy.setupEntries(testEntries, entryIdMap));
    cy.visit("/app");
  });

  it("should view entries in the Monthly Log correctly", () => {
    cy.get("#header-dropdown-container").click();
    cy.get("#header-dropdown-container ul[role=menu] li:nth-child(2)").click();
    cy.get("@testEntries").then(testEntries => {
      let currDate = new Date();
      for (const entry of testEntries) {
        let targetDate = new Date(entry.dateMonthlyLog);
        navigateToYearAndMonth(currDate, targetDate);
        let dayRow = cy.get(`#day-${targetDate.getDate()}`);
        dayRow.scrollIntoView({ duration: 500 });
        cy.get("div.text-container").contains(entry.text).should("have.length", 1);
      }
    });

    cy.wrap(true).as("finished");
  });

  after(() => {
    cy.get("@finished")
      .then(() => cy.get("@testEntries"))
      .then(testEntries => cy.destroyEntries(testEntries, entryIdMap));
  });
  
  function navigateToYearAndMonth(currentDate, targetDate) {
    if (yearAndMonthNotSame(currentDate, targetDate)) {
      cy.get("div.b-form-datepicker").click();
      const previousMonthBtn = cy.get(".b-calendar button[title='Previous month']");
      while (yearAndMonthNotSame(currentDate, targetDate)) {
        previousMonthBtn.click();
        currentDate.setMonth(currentDate.getMonth() - 1);
      }
      cy.get(`div[data-month] span`).contains("1").click().click();
    }
  }

  function yearAndMonthNotSame(date1, date2) {
    return date1.getFullYear() !== date2.getFullYear() ||
           date1.getMonth() !== date2.getMonth();
  }

});
