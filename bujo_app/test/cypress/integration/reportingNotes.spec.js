/// <reference types="cypress" />

describe("Reporting - Note list tests", () => {

  const entryIdMap = {};

  before(() => {
    cy.fixture("testEntries3.json").as("testEntries");
    cy.signin();
    cy.get("@testEntries").then(testEntries => cy.setupEntries(testEntries, entryIdMap));
    cy.visit("/app");
  })

  it('should display note list report correctly', () => {
    cy.get("#header-dropdown-container").click();
    cy.get("#header-dropdown-container ul[role=menu] li:nth-child(3)").click();
    cy.get("div[role=radiogroup] input[type=radio][value=NOTE_REPORT] + label").click();
    cy.get("#panel hr + button.btn-success").click();
    cy.get("#values-wrapper strong").should("contain.text", 2);
    cy.get("#notes-table tbody tr").should("have.length", 2);
    cy.get("span.square-badge").should("have.length", 1);

    cy.wrap(true).as("finished");
  });

  after(() => {
    cy.get("@finished").then(() => {
      cy.request("DELETE", `/api/entries/${entryIdMap["Online schůze"]}`);
      cy.request("DELETE", `/api/entries/${entryIdMap["Brainstorming"]}`);
    })
  });

})
