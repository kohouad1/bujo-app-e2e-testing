/// <reference types="cypress" />

import DailyLogPage from "../support/pages/DailyLogPage";

describe("Entry form validation (parameterized)", () => {

  const dailyLogPage = new DailyLogPage();

  before(() => {
    cy.fixture("entryFormValidation-mixed.json").as("entryData");
    cy.signin();
    dailyLogPage.visit();
  });

  it("should accordingly validate entry data", function () {
    dailyLogPage.openNewEntryDialog();

    this.entryData.forEach(entry => {
      dailyLogPage.typeEntryText(entry.entryText);
      dailyLogPage.setEntryType(entry.entryType);
      dailyLogPage.setTaskStatus(entry.markTaskAsCompleted);
      dailyLogPage.setEntryStatus(entry.markEntryAsCanceled);
      dailyLogPage.setSignifier(entry.setSignifier, entry.signifier);
      dailyLogPage.setCopyToMonthlyLog(entry.copyToMonthlyLog);

      dailyLogPage.getEntryTextInput()
                  .should(`${entry.validEntryText ? "not." : ""}have.class`,
                    "is-invalid");

      dailyLogPage.getCreateEntryButton()
                  .should(`${entry.validEntryText ? "not." : ""}have.attr`,
                    "disabled")

      dailyLogPage.resetNewEntryForm();
    });
  })

});
