/// <reference types="cypress" />

import DailyLogPage from "../support/pages/DailyLogPage";

describe("Editing entries tests", () => {

  const dailyLogPage = new DailyLogPage();
  const entryIdMap = {};
  const entriesToEdit = [
    { from: "Dojít nakoupit", to: "Dojít nakoupit potraviny" },
    { from: "Online schůze", to: "Online schůze na MS Teams" },
  ]

  before(() => {
    cy.fixture("testEntries1.json").as("testEntries");
    cy.signin();
    // setup and create entries programmatically
    cy.get("@testEntries").then(testEntries => cy.setupEntries(testEntries, entryIdMap));
    dailyLogPage.visit();
  });

  it("should successfully update entries", () => {
    for (const entryToEdit of entriesToEdit) {
      editEntry(entryToEdit);
      cy.get(`#${entryIdMap[entryToEdit.from]} div.text-container`)
        .should("contain.text", entryToEdit.to);
    }
    dailyLogPage.getEntryList().should("have.length", 4);
    cy.wrap(true).as("finished");
  });

  after(() => {
    cy.get("@finished").then(() => {
      cy.request("DELETE", `/api/entries/${entryIdMap["Dojít nakoupit"]}`);
      cy.request("DELETE", `/api/entries/${entryIdMap["Online schůze"]}`);
    });
  });

  function editEntry(entryToEdit) {
    cy.get("@idSet").then(() => {
      cy.get(`#${entryIdMap[entryToEdit.from]} button.btn-outline-primary`).click();
      cy.get(`#entry-text-input`).clear().type(entryToEdit.to);
      cy.get(`#edit-entry-dialog footer button.btn-primary`).click({ force: true });
    })
  }

});
