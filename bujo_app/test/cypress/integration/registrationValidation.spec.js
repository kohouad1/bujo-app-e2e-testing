/// <reference types="cypress" />

import HomePage from "../support/pages/HomePage";

describe("Registration test (parameterized)", () => {

  const homePage = new HomePage();

  before(() => {
    cy.fixture("registration-mixed.json").as("formInputData");
    homePage.visit();
    homePage.toggleFormType();
  });

  it("should accordingly validate registration user data", function () {
    this.formInputData.forEach(inputData => {
      const validUsername = inputData.validUsername,
            validPassword = inputData.validPassword,
            validPasswordAgain = inputData.validPasswordAgain;
      homePage.clearUsername();
      homePage.clearPassword();
      homePage.clearPasswordAgain();
      homePage.typeUsername(inputData.username);
      homePage.typePassword(inputData.password);
      homePage.typePasswordAgain(inputData.passwordAgain);

      assertInputValidity(homePage.getUsernameInput(), validUsername);
      assertInputValidity(homePage.getPasswordInput(), validPassword);
      assertInputValidity(homePage.getPasswordAgainInput(), validPasswordAgain);

      homePage.getSignUpButton()
              .should(`${validUsername && validPassword && validPasswordAgain ? "not." : ""}have.attr`,
                "disabled");
    });
  })

  function assertInputValidity(input, shouldBeValid) {
    input.should(`${shouldBeValid ? "not." : ""}have.class`, "is-invalid");
  }

});
