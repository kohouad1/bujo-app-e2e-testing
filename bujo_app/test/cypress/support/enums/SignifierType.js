const SignifierType = Object.freeze({
  IMPORTANT: "IMPORTANT",
  INSPIRATION: "INSPIRATION"
});

export default SignifierType;
