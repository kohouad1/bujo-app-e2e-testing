// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("signin", () => {
  cy.request("POST", "/api/users/signin",
    { username: "zks_user", password: "E2ETesting" })
    .then(response => expect(response.status).to.eq(200));
});

Cypress.Commands.add("setupEntries", (testEntries, entryIdMap) => {
  for (const entry of testEntries) {
    // set Daily Log to today's date so it always shows up on the page
    entry.dateDailyLog = new Date().toISOString().split("T")[0];
    if (entry.parent) {
      cy.get("@idSet")
        .then(id => cy.request("POST", `/api/entries/${id}/subentries`, entry));
    } else {
      cy.request("POST", `/api/entries`, entry)
        .then(req => {
          entryIdMap[entry.text] = req.body.resource._id;
          cy.wrap(entryIdMap[entry.text]).as("idSet");
        });
    }
  }
});

Cypress.Commands.add("destroyEntries", (testEntries, entryIdMap) => {
  for (const entry of testEntries) {
    if (entryIdMap[entry.text]) {
      cy.request("DELETE", `/api/entries/${entryIdMap[entry.text]}`);
    }
  }
});
