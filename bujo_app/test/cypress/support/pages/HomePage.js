class HomePage {

  constructor() {
    this.USERNAME_INPUT_SELECTOR = "#username-input";
    this.PASSWORD_INPUT_SELECTOR = "#password-input";
    this.PASSWORD_AGAIN_INPUT_SELECTOR = "#passwordAgain-input";
  }

  visit() {
    cy.visit("/home")
  }

  toggleFormType() {
    cy.get("#form-wrapper > a").click();
  }

  getUsernameInput() {
    return cy.get(this.USERNAME_INPUT_SELECTOR);
  }

  getPasswordInput() {
    return cy.get(this.PASSWORD_INPUT_SELECTOR);
  }

  getPasswordAgainInput() {
    return cy.get(this.PASSWORD_AGAIN_INPUT_SELECTOR);
  }

  getSignUpButton() {
    return cy.get("#form-wrapper > form button[type=submit]");
  }

  typeUsername(username) {
    this.getUsernameInput().type(username);
  }

  typePassword(password) {
    this.getPasswordInput().type(password);
  }

  typePasswordAgain(passwordAgain) {
    this.getPasswordAgainInput().type(passwordAgain);
  }

  clearUsername() {
    this.getUsernameInput().clear();
  }

  clearPassword() {
    this.getPasswordInput().clear();
  }

  clearPasswordAgain() {
    this.getPasswordAgainInput().clear();
  }

}

export default HomePage;
