class DailyLogPage {

  constructor() {
    this.ALL_ENTRIES_SELECTOR = ".entry";
    this.OPEN_NEW_ENTRY_DIALOG_BUTTON_SELECTOR = "#new-entry-btn-container > button[type=button]";
    this.NEW_ENTRY_DIALOG_SELECTOR = "#create-new-entry-dialog";
    this.ENTRY_TEXT_INPUT_SELECTOR = "#entry-text-input";
    this.TASK_COMPLETED_CHECKBOX_SELECTOR = "#task-completed-checkbox";
    this.ENTRY_CANCELED_CHECKBOX_SELECTOR = "#entry-canceled-checkbox";
    this.SET_SIGNIFIER_CHECKBOX_SELECTOR = "#set-signifier-checkbox";
    this.SET_COPY_TO_MONTHLY_LOG_CHECKBOX_SELECTOR = "#copy-to-monthly-log-checkbox";
    this.CREATE_ENTRY_BUTTON_SELECTOR = `${this.NEW_ENTRY_DIALOG_SELECTOR} footer button.btn-success`;
  }

  visit() {
    cy.visit("/app");
  }

  getEntryList() {
    return cy.get(this.ALL_ENTRIES_SELECTOR);
  }

  openNewEntryDialog() {
    cy.get(this.OPEN_NEW_ENTRY_DIALOG_BUTTON_SELECTOR).click();
  }

  openNewSubentryDialog() {
    cy.get("@idSet").then(id => {
      cy.get(`#${id} button.btn-outline-success`).click();
    })
  }

  getEntryTextInput() {
    return cy.get(this.ENTRY_TEXT_INPUT_SELECTOR);
  }

  getCreateEntryButton() {
    return cy.get(this.CREATE_ENTRY_BUTTON_SELECTOR);
  }

  typeEntryText(text) {
    this.getEntryTextInput().type(text);
  }

  setEntryType(type) {
    cy.get(`${this.NEW_ENTRY_DIALOG_SELECTOR} input[type=radio][value=${type}]`)
      .click({ force: true })
  }

  setTaskStatus(asCompleted) {
    if (asCompleted) cy.get(this.TASK_COMPLETED_CHECKBOX_SELECTOR).click({ force: true });
  }

  setEntryStatus(asCanceled) {
    if (asCanceled) cy.get(this.ENTRY_CANCELED_CHECKBOX_SELECTOR).click({ force: true });
  }

  setSignifier(asSet, signifier) {
    if (asSet) {
      cy.get(this.SET_SIGNIFIER_CHECKBOX_SELECTOR).click({ force: true });
      cy.get(`${this.NEW_ENTRY_DIALOG_SELECTOR} input[type=radio][value=${signifier}]`)
        .click({ force: true });
    }
  }

  setCopyToMonthlyLog(asSet) {
    if (asSet) {
      cy.get(this.SET_COPY_TO_MONTHLY_LOG_CHECKBOX_SELECTOR).click({ force: true });
    }
  }

  resetNewEntryForm() {
    this.getEntryTextInput().clear();
    this.setEntryType("TASK");

    // reset checkboxes
    [this.TASK_COMPLETED_CHECKBOX_SELECTOR,
     this.ENTRY_CANCELED_CHECKBOX_SELECTOR,
     this.SET_SIGNIFIER_CHECKBOX_SELECTOR,
     this.SET_COPY_TO_MONTHLY_LOG_CHECKBOX_SELECTOR].forEach(selector => {
       cy.get(selector)
         .then($checkbox => $checkbox.prop("checked", false));
    });
  }

}

export default DailyLogPage;
